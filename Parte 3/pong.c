#include <stdio.h>
#include <stdlib.h>

#ifdef WIN32
#include <windows.h>
#elif _POSIX_C_SOURCE >= 199309L
#include <time.h>   // for nanosleep
#else
#include <unistd.h> // for usleep
#endif

void sleep(int milliseconds) // cross-platform sleep function
{
	#ifdef WIN32
	    Sleep(milliseconds);
	#elif _POSIX_C_SOURCE >= 199309L
	    struct timespec ts;
	    ts.tv_sec = milliseconds / 1000;
	    ts.tv_nsec = (milliseconds % 1000) * 1000000;
	    nanosleep(&ts, NULL);
	#else
	    usleep(milliseconds * 1000);
	#endif
}

#define MAXX 60//32
#define MAXY 24
#define BARRAX 10
#define BARRAY 2
#define BOLA 2

typedef struct{
    int coord_x;
    int coord_y;
    int largura;
}bola;

typedef struct{
    int coord_x;
    int largura;
}barra;

char tabuleiro[MAXY][MAXX];
int incremento_x = 1;
int incremento_y = 1;

int incremento_barra1 = 1;
int incremento_barra2 = -1;

int avanco1 = 1;
int avanco2 = 1;

bola bola1;
barra barra1;
barra barra2;
 
void setup(){
    for(int i = 0; i<=MAXY;i++){
        for(int j = 0; j<MAXX;j++){
            tabuleiro[i][j] = ' ';
        }
    }  

    barra1.coord_x = 2;
    barra1.largura = 5;
    barra2.coord_x = 2;
    barra2.largura = 5;

    bola1.coord_y = 10;
    bola1.coord_x = 5;
    bola1.largura = BOLA;

    for(int i = 0; i<=barra1.largura;i++){
        tabuleiro[0][barra1.coord_x+i] = '_';
    }

    for(int i = 0; i<=barra2.largura;i++){
        tabuleiro[MAXY-1][barra2.coord_x+i] = '_';
    }
}

void imprimir(){
    for(int i = 0; i<MAXX;i++){
        printf("*");
    }
    printf("**\n");

    for(int i = 0; i<MAXY;i++){
        printf("*");
        for(int j = 0; j<MAXX;j++){
            printf("%c",tabuleiro[i][j]);
        }
        printf("*\n");
    }

    for(int i = 0; i<MAXX;i++){
        printf("*");
    }
    printf("**\n");
}

void atualiza_bola(){
    if( (bola1.coord_y + incremento_y) >= MAXY || (bola1.coord_y + incremento_y)<0 || tabuleiro[bola1.coord_y+incremento_y][bola1.coord_x+incremento_x]=='_'){ //|| ((bola1.coord_x>=barra1.coord_x) && (bola1.coord_x<=(barra1.coord_x + barra1.largura)) && bola1.coord_y == 0)){
            incremento_y = incremento_y * (-1);
    }
    if( (bola1.coord_x + incremento_x) >= MAXX || (bola1.coord_x + incremento_x) < 0){
            incremento_x = incremento_x * (-1);
    }

    tabuleiro[bola1.coord_y][bola1.coord_x] = ' ';

    bola1.coord_x = bola1.coord_x + incremento_x;
    bola1.coord_y = bola1.coord_y + incremento_y;
    
    tabuleiro[bola1.coord_y][bola1.coord_x] = '0'; 
}

void atualiza_barras(){
    if(avanco1>0 && barra1.coord_x+barra1.largura+avanco1<MAXX){
        tabuleiro[0][barra1.coord_x] = ' ';
        barra1.coord_x += avanco1;
        tabuleiro[0][barra1.coord_x+barra1.largura] = '_';
    }
    else if(avanco1<0 && barra1.coord_x+avanco1>=0){
        tabuleiro[0][barra1.coord_x+barra1.largura] = ' ';
        barra1.coord_x += avanco1;
        tabuleiro[0][barra1.coord_x] = '_';
    }
    // para andar sozinha
    else{
        avanco1 = (-1)*avanco1;
    }

    if(avanco2>0 && barra2.coord_x+barra2.largura+avanco2<MAXX){
        tabuleiro[MAXY-1][barra2.coord_x] = ' ';
        barra2.coord_x += avanco2;
        tabuleiro[MAXY-1][barra2.coord_x+barra2.largura] = '_';
    }
    else if(avanco2<0 && barra2.coord_x+avanco2>=0){
        tabuleiro[MAXY-1][barra2.coord_x+barra2.largura] = ' ';
        barra2.coord_x += avanco2;
        tabuleiro[MAXY-1][barra2.coord_x] = '_';
    }
    // para andar sozinha
    else{
        avanco2 = (-1)*avanco2;
    }   
}
 
int main(){
       
    setup();

    for(int k = 0; k<100; k++){
        atualiza_barras();
        atualiza_bola();
        system("clear");
        imprimir();
        sleep(100);
    }
    return 0;
}